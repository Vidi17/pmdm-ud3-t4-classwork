package dam.android.vidal.u3t4event;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener{

    private TextView tvEventName, tvDate, tvTime;
    private DialogFragment dateFragment, timeFragment;
    private RadioGroup rgPriority;
    private Button btAccept;
    private Button btCancel;
    private EditText etPlace;
    private String[] months;

    private String hourString, minuteString;

    int year, month, day;

    private String priority = "Normal";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();

        Bundle inputData = getIntent().getExtras();

        tvEventName.setText(inputData.getString("EventName"));
    }

    private void setUI(){
        Bundle bundle = getIntent().getExtras();

        tvEventName = findViewById(R.id.tvEventName);
        tvEventName.setText(this.getIntent().getStringExtra("EventName"));

        tvDate = findViewById(R.id.tvDate);

        if (bundle.getString("Day") != null) {
            tvDate.setText(String.format("%s/%s/%s", bundle.getString("Day")
                    , bundle.getString("Month"), bundle.getString("Year")));
        }else {
            tvDate.setText(obtenerFechaActual());
        }

        tvTime = findViewById(R.id.tvTime);
        if (bundle.getString("Hour") != null) {
            tvTime.setText(String.format("%s:%s", bundle.getString("Hour")
                    , bundle.getString("Minute")));
        }else {
            tvTime.setText(obtenerHoraActual());
        }

        etPlace = findViewById(R.id.etPlace);
        etPlace.setText(bundle.getString("Place"));

        rgPriority = findViewById(R.id.rgPriority);
        if (bundle.getString("Priority") != null) {
            restoreCheck(bundle.getString("Priority"));
        }else{
            rgPriority.check(R.id.rbNormal);
        }

        addDateFragment();
        addTimeFragment();
        btAccept = findViewById(R.id.btAccept);
        btCancel = findViewById(R.id.btCancel);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);

        Resources res = getResources();
        months = res.getStringArray(R.array.months);
    }

    private void restoreCheck(String string){
        if (string.equals(getString(R.string.rbLow))){
            rgPriority.check(R.id.rbLow);
        }else if (string.equals(getString(R.string.rbHigh))){
            rgPriority.check(R.id.rbHigh);
        }else{
            rgPriority.check(R.id.rbNormal);
        }
    }

    @Override
    public void onClick(View v) {
        Intent activityResult = new Intent();
        Resources res = getResources();
        Bundle eventData = new Bundle();
        if (hourString == null){
            hourString = "00";
        }
        if (minuteString == null){
            minuteString = "00";
        }

        switch (v.getId()){
            case R.id.btAccept:
                eventData.putString("EventData", res.getString(R.string.tvPriority) + ": " + priority + "\n" +
                        res.getString(R.string.tvPlace) + ": " + etPlace.getText() + "\n" +
                        res.getString(R.string.hour) + ": " + hourString + ":"
                        + minuteString + "\n" +
                        res.getString(R.string.month) + ": " + months[month] + "\n" +
                        res.getString(R.string.day) + ": " + day + "\n" +
                        res.getString(R.string.year) + ": " + year);

                eventData.putString("Place", etPlace.getText().toString());
                eventData.putString("Hour", hourString);
                eventData.putString("Minute", minuteString);
                eventData.putString("Priority", priority);
                eventData.putString("Day", String.valueOf(day));
                eventData.putString("Month", String.valueOf(month+1));
                eventData.putString("Year", String.valueOf(year));
                break;
            case R.id.btCancel:
                eventData.putString("EventData", this.getIntent().getStringExtra("EventData"));
                break;
        }
        activityResult.putExtras(eventData);
        setResult(RESULT_OK, activityResult);

        finish();
    }

    protected void setDate(int year, int month, int day){
        this.year = year;
        this.month = month;
        this.day = day;
        String monthString = formatNumber(month+1);
        String dayString = formatNumber(day);

        tvDate.setText(String.format("%s/%s/%d", dayString, monthString, year));
    }

    protected void setTime(int hour, int minute){
        hourString = formatNumber(hour);
        minuteString = formatNumber(minute);

        tvTime.setText(String.format("%s:%s", hourString, minuteString));
    }

    private String formatNumber(int number){
        if (number < 10){
            return  "0" + number;
        }else{
            return String.valueOf(number);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        Resources res = getResources();
        switch (i) {
            case R.id.rbLow:
                priority = res.getString(R.string.rbLow);
                break;
            case R.id.rbNormal:
                priority = res.getString(R.string.rbNormal);
                break;
            case R.id.rbHigh:
                priority = res.getString(R.string.rbHigh);
                break;
        }
    }

    public void showTimePickerDialog(View v){
        Bundle bundle = new Bundle();
        bundle.putString("Hour", hourString);
        bundle.putString("Minute", minuteString);

        DialogFragment newFragment = new TimeFragment();
        newFragment.setArguments(bundle);

        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public void showDatePickerDialog(View v){
        Bundle bundle = new Bundle();
        bundle.putString("Day", String.valueOf(day));
        bundle.putString("Month", String.valueOf(month));
        bundle.putString("Year", String.valueOf(year));

        DialogFragment newFragment = new DateFragment();

        newFragment.setArguments(bundle);
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void addDateFragment() {
        dateFragment = DateFragment.newInstance();


        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .addToBackStack(null)

                .replace(R.id.dateFragment, dateFragment, null)

                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)

                .commit();
    }

    public void addTimeFragment() {
        timeFragment = TimeFragment.newInstance();


        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .addToBackStack(null)

                .replace(R.id.timeFragment, timeFragment, null)

                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)

                .commit();
    }

    public static String obtenerHoraActual() {
        String formato = "HH:mm";
        return obtenerFechaConFormato(formato);
    }

    public static String obtenerFechaActual() {
        String formato = "dd/MM/yyyy";
        return obtenerFechaConFormato(formato);
    }

    @SuppressLint("SimpleDateFormat")
    public static String obtenerFechaConFormato(String formato) {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat(formato);
        sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("ECT")));
        return sdf.format(date);
    }
}