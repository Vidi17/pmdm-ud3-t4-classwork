package dam.android.vidal.u3t4event;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

public class U3T4EventActivity extends AppCompatActivity {

    private EditText etEventName;
    private TextView tvCurrentData;
    private ActivityResultLauncher<Intent> eventActivityResult;

    String place, day, hour, minute, month, year, priority;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_u3_t4_event);

        setUI();

        registerForEventResult();
    }

    private void  setUI(){
        etEventName = findViewById(R.id.etEventName);
        tvCurrentData = findViewById(R.id.tvCurrentData);

        tvCurrentData.setText(" ");
    }

    private void registerForEventResult(){
        eventActivityResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK){
                        Intent data = result.getData();
                        if (data != null){
                            tvCurrentData.setText(data.getStringExtra("EventData"));
                            place = data.getStringExtra("Place");
                            day = data.getStringExtra("Day");
                            hour = data.getStringExtra("Hour");
                            minute = data.getStringExtra("Minute");
                            month = data.getStringExtra("Month");
                            year = data.getStringExtra("Year");
                            priority = data.getStringExtra("Priority");
                        }
                    }
                });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putString("eventData", tvCurrentData.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        String eventData = savedInstanceState.getString("eventData");
        tvCurrentData.setText(eventData);
    }

    public void editEventData(View v){
        Intent intent = new Intent(this, EventDataActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString("EventName", etEventName.getText().toString());
        bundle.putString("EventData", tvCurrentData.getText().toString());
        bundle.putString("Place", place);
        bundle.putString("Minute", minute);
        bundle.putString("Hour", hour);
        bundle.putString("Day", day);
        bundle.putString("Month", month);
        bundle.putString("Year", year);
        bundle.putString("Priority", priority);

        intent.putExtras(bundle);

        eventActivityResult.launch(intent);
    }
}