package dam.android.vidal.u3t4event;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class DateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    private static String LOG_TAG = "DATE_FRAGMENT";

    public DateFragment() {
    }

    public static DateFragment newInstance() {
        DateFragment dateFragment = new DateFragment();
        return dateFragment;
    }

    // onAttach: it's called when fragment is associated to this activity  (context)
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        Log.d(LOG_TAG, "onAttach(): Fragment associated to Activity");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);


        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {

        ((EventDataActivity) getActivity()).setDate(year, month, day);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(LOG_TAG, "onCreateView(): Fragment UI - inflating ");

        return inflater.inflate(R.layout.date_fragment, container, false);
    }
}
